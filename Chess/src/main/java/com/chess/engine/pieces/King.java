package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class King extends Piece {
	/**
	 *
	 * @param colour Colour of the King.
	 * @param position King's position.
	 * @param isFirstMove Is King's first move
	 */
	public King(Colour colour, int position, boolean isFirstMove) {
		super(PieceType.KING, colour, position, isFirstMove);
	}

	/**
	 *
	 * @return True if it is first move.
	 */
	@Override
	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	/**
	 *
	 * @param board This is chessboard.
	 * @return All possible moves.
	 */
	@Override
	public Collection<Move> getPossibleMoves(Board board) {
		int[] positionChange = {-7, 1, 9, -8, 8, 7, -1, -9};
		Collection<Move> possibleMoves = new ArrayList<>();
		for (int i = 0; i < 8; i++) {
			if (0 <= this.position + positionChange[i] && this.position + positionChange[i] < 64) {
				if ((position % 8 == 0 && i < 5) || (position % 8 == 7 && i >= 3 && i < 8) || (position % 8 != 0 && position % 8 != 7)) {
					Tile destinationTile = board.getTile(this.position + positionChange[i]);
					if (destinationTile.isTileOccupied() == true && destinationTile.getPiece().getColour() != this.colour) {
						Move attackingMove = new Move.AttackingMove(board, this, this.position + positionChange[i], destinationTile.getPiece());
						possibleMoves.add(attackingMove);
					} else if (destinationTile.isTileOccupied() == false) {
						Move normalMove = new Move.NormalMove(board, this, this.position + positionChange[i]);
						possibleMoves.add(normalMove);
					}
				}
			}
		}
		return possibleMoves;
	}

	/**
	 *
	 * @return Abbreviation of the type.
	 */
	@Override
	public String toString(){
		return PieceType.KING.toString();
	}

	/**
	 *
	 * @param move This is the definition of move of the piece.
	 * @return New king for move.
	 */
	@Override
	public King movePiece(Move move) {
		return new King(move.getMovedPiece().getColour(), move.getDestinationTile(), false);
	}
}
