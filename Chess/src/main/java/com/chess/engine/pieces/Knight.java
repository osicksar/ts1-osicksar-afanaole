package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;

import java.util.ArrayList;
import java.util.Collection;

public class Knight extends Piece {
	/**
	 *
	 * @param colour Colour of the knight.
	 * @param position Knight's position.
	 * @param isFirstMove Is Knight's first move
	 */
	public Knight(Colour colour, int position, boolean isFirstMove) {
		super(PieceType.KNIGHT, colour, position, isFirstMove);
	}

	@Override
	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	/**
	 *
	 * @param board This is chessboard.
	 * @return All possible moves.
	 */
	@Override
	public Collection<Move> getPossibleMoves(Board board) {
		int[] positionChange = {-6, 10, -15, 17, -17, 15, -10, 6};
		Collection<Move> possibleMoves = new ArrayList<>();
		for (int i = 0; i < 8; i++) {
			if (0 <= this.position + positionChange[i] && this.position + positionChange[i] < 64) {
				if ((this.position % 8 == 0 && i < 4) || (this.position % 8 == 7 && i >= 4 && i < 8) || (this.position % 8 == 6 && i > 1 )|| (this.position % 8 == 1 && i < 6 ) || (this.position % 8 != 0 && this.position % 8 != 7 && this.position % 8 != 1 && this.position % 8 != 6)){
					Tile destinationTile = board.getTile(this.position + positionChange[i]);
					if (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() != this.colour) {
						Move attackingMove = new Move.AttackingMove(board, this, this.position + positionChange[i], destinationTile.getPiece());
						possibleMoves.add(attackingMove);
					}else {
						Move normalMove = new Move.NormalMove(board, this, this.position + positionChange[i]);
						possibleMoves.add(normalMove);
					}
				}
			}
		}
		return possibleMoves;
	}

	/**
	 *
	 * @return Abbreviation of the type.
	 */
	@Override
	public String toString(){
		return PieceType.KNIGHT.toString();
	}

	/**
	 *
	 * @param move This is the definition of move of the piece.
	 * @return New knight for move.
	 */
	@Override
	public Knight movePiece(Move move) {
		return new Knight(move.getMovedPiece().getColour(), move.getDestinationTile(), false);
	}
}
