package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;

import java.util.ArrayList;
import java.util.Collection;

import static com.chess.engine.Colour.black;
import static com.chess.engine.Colour.white;

// TODO: en passant!!!

public class Pawn extends Piece {
	/**
	 *
	 * @param colour Colour of the pawn.
	 * @param position Pawn's position.
	 * @param isFirstMove Is Pawn's first move
	 */
	public Pawn(Colour colour, int position, boolean isFirstMove) {
		super(PieceType.PAWN, colour, position, isFirstMove);
		this.isFirstMove = true;
	}

	/**
	 *
	 * @return True if it is first move.
	 */
	@Override
	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	/**
	 *
	 * @param board This is chessboard.
	 * @return All possible moves.
	 */
	@Override
	public Collection<Move> getPossibleMoves(Board board) {
		int[] positionChangeWhite = {-8, -16};
		int[] positionChangeBlack = {8, 16};
		int[] isOccupiedWhite = {-7, -9};
		int[] isOccupiedBlack = {9, 7};
		Collection<Move> possibleMoves = new ArrayList<>();
		//*Black pawn *//
		if (this.colour == black) {
			//* Classic Move *//
			if (0 <= this.position + positionChangeBlack[0] && this.position + positionChangeBlack[0] < 64) {
				Tile destinationTile = board.getTile(this.position + positionChangeBlack[0]);
				if (!destinationTile.isTileOccupied()) {
					Move pawnMove = new Move.PawnMove(board, this, this.position + positionChangeBlack[0]);
					possibleMoves.add(pawnMove);
				}
			}

			//* First Move *//
			if (7 < this.position && this.position < 16) {
				Tile destinationTile = board.getTile(this.position + positionChangeBlack[1]);
				if (!destinationTile.isTileOccupied()) {
					Tile destinationTile2 = board.getTile(this.position + positionChangeBlack[0]);
					if (!destinationTile2.isTileOccupied()) {
						Move pawnPush = new Move.PawnPush(board, this, this.position + positionChangeBlack[1]);
						possibleMoves.add(pawnPush);
					}
				}
			}

			//* Attack Move *//
			for (int i = 0; i < 2; i++) {
				if (0 <= this.position + isOccupiedBlack[i] && this.position + isOccupiedBlack[i] < 64) {
					if ((this.position % 8 == 0 && i == 0) || (this.position % 8 == 7 && i >= 3 && i == 1) || (this.position % 8 != 0 && this.position % 8 != 7)) {
						Tile destinationTile = board.getTile(this.position + isOccupiedBlack[i]);
						if (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() != this.colour) {
							Move pawnAttackingMove = new Move.PawnAttackMove(board, this, this.position + isOccupiedBlack[i], destinationTile.getPiece());
							possibleMoves.add(pawnAttackingMove);
						}
						else if(board.getEnPassantPawn() != null){
							if(board.getEnPassantPawn().getPosition() == this.position - 1 || board.getEnPassantPawn().getPosition() == this.position + 1){
								Move pawnAttackingMove = new Move.PawnEnPassant(board, this, this.position + isOccupiedBlack[i], board.getEnPassantPawn());
								possibleMoves.add(pawnAttackingMove);
							}
						}
					}
				}
			}
		}

		//* White pawn *//
		if (this.colour == white) {
			//* Classic Move *//
			if (0 <= this.position + positionChangeWhite[0] && this.position + positionChangeWhite[0] < 64) {
				Tile destinationTile = board.getTile(this.position + positionChangeWhite[0]);
				if (destinationTile.isTileOccupied() == false) {
					Move pawnMove = new Move.PawnMove(board, this, this.position + positionChangeWhite[0]);
					possibleMoves.add(pawnMove);
				}
			}

			//* First Move *//
			if (47 < this.position && this.position < 56) {
				Tile destinationTile = board.getTile(this.position + positionChangeWhite[1]);
				if (destinationTile.isTileOccupied() == false) {
					Tile destinationTile2 = board.getTile(this.position + positionChangeWhite[0]);
					if (destinationTile2.isTileOccupied() == false) {
						Move pawnPush = new Move.PawnPush(board, this, this.position + positionChangeWhite[1]);
						possibleMoves.add(pawnPush);
					}
				}
			}

			//* Attack Move *//
			for (int i = 0; i < 2; i++) {
				if (0 <= this.position + isOccupiedWhite[i] && this.position + isOccupiedWhite[i] < 64) {
					if ((this.position % 8 == 0 && i == 0) || (this.position % 8 == 7 && i >= 3 && i == 1) || (this.position % 8 != 0 && this.position % 8 != 7)) {
						Tile destinationTile = board.getTile(this.position + isOccupiedWhite[i]);
						if (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() != this.colour) {
							Move pawnAttackingMove = new Move.PawnAttackMove(board, this, this.position + isOccupiedWhite[i], destinationTile.getPiece());
							possibleMoves.add(pawnAttackingMove);
						}
						else if(board.getEnPassantPawn() != null){
							if(board.getEnPassantPawn().getPosition() == this.position - 1 || board.getEnPassantPawn().getPosition() == this.position + 1){
								Move pawnAttackingMove = new Move.PawnEnPassant(board, this, this.position + isOccupiedWhite[i], board.getEnPassantPawn());
								possibleMoves.add(pawnAttackingMove);

							}
						}
					}
				}
			}
		}
		return possibleMoves;
	}

	/**
	 *
	 * @return Abbreviation of the type.
	 */
	@Override
	public String toString(){
		return PieceType.PAWN.toString();
	}

	/**
	 *
	 * @param move This is the definition of move of the piece.
	 * @return New pawn for move.
	 */
	@Override
	public Pawn movePiece(Move move) {
		return new Pawn(move.getMovedPiece().getColour(), move.getDestinationTile(), false);
	}
}
