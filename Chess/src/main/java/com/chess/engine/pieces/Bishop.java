package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;

import java.util.ArrayList;
import java.util.Collection;

public class Bishop extends Piece {
	/**
	 *
	 * @param colour Colour of the bishop.
	 * @param position Bishop's position.
	 * @param isFirstMove Is Bishop's first move
	 */
	public Bishop(Colour colour, int position, boolean isFirstMove) {
		super(PieceType.BISHOP, colour, position, isFirstMove);;
	}

	@Override
	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	/**
	 *
	 * @param board This is chessboard.
	 * @return All possible moves.
	 */
	@Override
	public Collection<Move> getPossibleMoves(Board board) {
		int[] positionChange = {-7, 9, -9, 7};
		int tempPos = this.position;
		Collection<Move> possibleMoves = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			if((tempPos + positionChange[i] >= 0) && (tempPos + positionChange[i] <64)) {
				Tile destinationTile = board.getTile(this.position + positionChange[i]);
				while (((!destinationTile.isTileOccupied()) || (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() != this.colour))) {
					if ((tempPos + positionChange[i] >= 0) && (tempPos + positionChange[i] < 64)) {
						destinationTile = board.getTile(tempPos + positionChange[i]);
						if (0 <= tempPos + positionChange[i] && tempPos + positionChange[i] < 64) {
							if ((tempPos % 8 == 0 && i < 2) || (tempPos % 8 == 7 && i >= 2 && i < 4) || (tempPos % 8 != 0 && tempPos % 8 != 7)) {
								tempPos += positionChange[i];
								if (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() != this.colour) {
									Move attackingMove = new Move.AttackingMove(board, this, tempPos, destinationTile.getPiece());
									possibleMoves.add(attackingMove);
									break;
								} else if (destinationTile.isTileOccupied() && destinationTile.getPiece().getColour() == this.colour) {
									break;
								}
								Move normalMove = new Move.NormalMove(board, this, tempPos);
								possibleMoves.add(normalMove);
							} else {
								break;
							}
						}
					} else {
						break;
					}
				}
				tempPos = this.position;
			}
		}
		return possibleMoves;
	}

	/**
	 *
	 * @param move This is the definition of move of the piece.
	 * @return New bishop for move.
	 */
	@Override
	public Bishop movePiece(Move move) {
		return new Bishop(move.getMovedPiece().getColour(), move.getDestinationTile(), false);
	}

	/**
	 *
	 * @return Abbreviation of the type.
	 */
	@Override
	public String toString(){
		return PieceType.BISHOP.toString();
	}
}