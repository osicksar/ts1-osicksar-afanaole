package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;

import java.util.Collection;

public abstract class Piece {
	protected Colour colour;
	protected int position;
	protected PieceType pieceType;
	protected boolean isFirstMove = true;
	private int cachedHashCode;


	public Piece(PieceType pieceType, Colour colour, int position, boolean isFirstMove) {
		this.pieceType = pieceType;
		this.colour = colour;
		this.position = position;
		this.cachedHashCode = computeHashCode();
		this.isFirstMove = isFirstMove;
	}

	private int computeHashCode(){
		int result = pieceType.hashCode();
		result = 31 * result + colour.hashCode();
		result = 31 * result + position;
		result = 31 * result + (isFirstMove ? 1:0);
		return result;
	};
	public Collection<Move> getPossibleMoves(Board board) {
		return null;
	}

	public Colour getColour() {
		return colour;
	}

	public int getPosition() {
		return position;
	}

	@Override
	public int hashCode() {
		return this.cachedHashCode;
	}


	@Override
	public boolean equals(Object isEqual){
		if(this == isEqual){
			return true;
		}
		if(!(isEqual instanceof Piece)){
			return false;
		}
		Piece otherPiece = (Piece) isEqual;
		return position == otherPiece.getPosition() && pieceType == otherPiece.getPieceType() && colour == otherPiece.getColour();
	}
	public enum PieceType{
		PAWN("P"){
			@Override
			public boolean isKing(){
				return false;
			}
			@Override
			public boolean isRook() {
				return false;
			}
		},
		KNIGHT("N") {
			@Override
			public boolean isKing() {
				return false;
			}
			@Override
			public boolean isRook() {
				return false;
			}
		},
		BISHOP("B") {
			@Override
			public boolean isKing() {
				return false;
			}
			@Override
			public boolean isRook() {
				return false;
			}
		},
		ROOK("R") {
			@Override
			public boolean isKing() {
				return false;
			}
			@Override
			public boolean isRook() {
				return true;
			}
		},
		KING("K") {
			@Override
			public boolean isKing() {
				return true;
			}
			@Override
			public boolean isRook() {
				return false;
			}
		},
		QUEEN("Q") {
			@Override
			public boolean isKing() {
				return false;
			}
			@Override
			public boolean isRook() {
				return false;
			}
		};

		private final String pieceName;

		PieceType(String pieceName){
			this.pieceName = pieceName;
		}

		@Override
		public String toString(){
			return this.pieceName;
		}

		public abstract boolean isKing();
		public abstract boolean isRook();
	}

	public boolean isFirstMove() {
		return this.isFirstMove;
	}

	public abstract Piece movePiece(Move move);

	public PieceType getPieceType() {
		return pieceType;
	}
}
