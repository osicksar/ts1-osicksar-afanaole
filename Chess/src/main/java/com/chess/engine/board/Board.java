package com.chess.engine.board;

import com.chess.engine.Colour;
import com.chess.engine.pieces.*;
import com.chess.engine.player.BlackPlayer;
import com.chess.engine.player.Player;
import com.chess.engine.player.WhitePlayer;

import java.util.*;

public class Board {
    private Map<Integer, Piece> boardConfiguration;
    private List<Tile> chessBoard;
    private Collection<Piece> whitePieces;
    private Collection<Piece> blackPieces;
    private WhitePlayer whitePlayer;
    private BlackPlayer blackPlayer;
    private Player currentPlayer;
    private Pawn enPassantPawn;

    private Board(Builder builder){
        this.boardConfiguration = Collections.unmodifiableMap(builder.boardConfiguration);
        this.chessBoard = createChessBoard(builder);
        this.enPassantPawn = builder.enPassantPawn;
        this.whitePieces = calculateActivePieces(this.chessBoard, Colour.white);
        this.blackPieces = calculateActivePieces(this.chessBoard, Colour.black);
        Collection<Move> baseWhiteLegalMoves = calculateLegalMoves(this.whitePieces);
        Collection<Move> baseBlackLegalMoves = calculateLegalMoves(this.blackPieces);
        this.whitePlayer = new WhitePlayer(this, baseWhiteLegalMoves, baseBlackLegalMoves);
        this.blackPlayer = new BlackPlayer(this, baseWhiteLegalMoves, baseBlackLegalMoves);
        this.currentPlayer = builder.nextPlayer.choosePlayer(this.whitePlayer, this.blackPlayer);
    }

    private Collection<Move> calculateLegalMoves(Collection<Piece> pieces) {
        List<Move> legalMoves = new ArrayList<>();
        for(Piece piece : pieces){
            legalMoves.addAll(piece.getPossibleMoves(this));
        }
        return legalMoves;
    }

    public Pawn getEnPassantPawn() {
        return this.enPassantPawn;
    }

    public Player currentPlayer(){
        return this.currentPlayer;
    }

    private Collection<Piece> calculateActivePieces(List<Tile> chessBoard, Colour colour) {
        List<Piece> activePieces = new ArrayList<>();
        for (Tile tile : chessBoard) {
            if(tile != null) {
                if (tile.isTileOccupied()) {
                    Piece piece = tile.getPiece();
                    if (piece.getColour() == colour) {
                        activePieces.add(piece);
                    }
                }
            }
        }
        return activePieces;
    }

    public Tile getTile(int tileCoordinate){
        return chessBoard.get(tileCoordinate);
    }

    /**
     *
     * @param builder sets up the board
     * @return list of 64 tiles
     */
    public static List<Tile> createChessBoard(Builder builder){
        Tile[] tiles = new Tile[64];
        List<Tile> list = new ArrayList<>();
        for(int i = 0; i<64; i++){
            tiles[i] = Tile.createTile(i, builder.boardConfiguration.get(i));
            list.add(tiles[i]);
        }
        return list;
    }
    public Collection<Piece> getWhitePieces(){
        return whitePieces;
    }
    public Collection<Piece> getBlackPieces(){
        return blackPieces;
    }

    /**
     *
     * @return setup standard chessboard
     */
    public static Board createStandardBoard(){
        Builder builder = new Builder();
        //White
        builder.setPiece(new Rook(Colour.black, 0, true));
        builder.setPiece(new Knight(Colour.black, 1, true));
        builder.setPiece(new Bishop(Colour.black, 2, true));
        builder.setPiece(new Queen(Colour.black, 3, true));
        builder.setPiece(new King(Colour.black, 4, true));
        builder.setPiece(new Bishop(Colour.black, 5, true));
        builder.setPiece(new Knight(Colour.black, 6, true));
        builder.setPiece(new Rook(Colour.black, 7, true));
        for(int i = 8; i < 16; i++)
        {
            builder.setPiece(new Pawn(Colour.black, i, true));
        }
        //Black
        builder.setPiece(new Rook(Colour.white, 56, true));
        builder.setPiece(new Knight(Colour.white, 57, true));
        builder.setPiece(new Bishop(Colour.white, 58, true));
        builder.setPiece(new Queen(Colour.white, 59, true));
        builder.setPiece(new King(Colour.white, 60, true));
        builder.setPiece(new Bishop(Colour.white, 61, true));
        builder.setPiece(new Knight(Colour.white, 62, true));
        builder.setPiece(new Rook(Colour.white, 63, true));
        for(int i = 48; i < 56; i++)
        {
            builder.setPiece(new Pawn(Colour.white, i, true));
        }
        //White to move
        builder.setPlayer(Colour.white);
        return builder.build();
    }

    public Player whitePlayer() {
        return whitePlayer;
    }
    public Player blackPlayer() {
        return blackPlayer;
    }

    /**
     *
     * @return all possible moves
     */
    public Collection<Move> getAllLegalMoves() {
        Collection<Move> allLegalMoves = calculateLegalMoves(this.whitePieces);
        allLegalMoves.addAll(calculateLegalMoves(this.blackPieces));
        allLegalMoves.addAll(whitePlayer().calculateKingCastles(calculateLegalMoves(this.whitePieces), calculateLegalMoves(this.blackPieces)));
        allLegalMoves.addAll(blackPlayer().calculateKingCastles(calculateLegalMoves(this.blackPieces), calculateLegalMoves(this.whitePieces)));
        return allLegalMoves;
    }

    /**
     * Builder sets up players and pieces on board
     */
    public static class Builder{
        Map<Integer, Piece> boardConfiguration;
        Colour nextPlayer;
        Pawn enPassantPawn;
        public Builder(){
            this.boardConfiguration = new HashMap<>(32, 1.0f);
        }
        public Builder setPiece(Piece piece){
            this.boardConfiguration.put(piece.getPosition(), piece);
            return this;
        }
        public Builder setPlayer(Colour nextPlayer){
            this.nextPlayer = nextPlayer;
            return this;
        }

        /**
         *
         * @return builds a Board with given parameters
         */
        public Board build(){
            return new Board(this);
        }

        public void setEnPassant(Pawn movedPawn) {
            this.enPassantPawn = movedPawn;
        }
    }
}
