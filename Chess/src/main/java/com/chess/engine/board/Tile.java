package com.chess.engine.board;

import com.chess.engine.pieces.Piece;
import java.util.HashMap;
import java.util.Map;

public abstract class Tile {
    protected final int tileCoordinate;

    private static final Map<Integer, EmptyTile> emptyTiles = createAllEmptyTiles();

    private static Map<Integer, EmptyTile> createAllEmptyTiles() {
        final Map<Integer, EmptyTile> emptyTileMap = new HashMap<>();
        for (int i = 0; i<64; i++){
            emptyTileMap.put(i, new EmptyTile(i));
        }
        return emptyTileMap;
    }


    private Tile(int tileCoordinate){
        this.tileCoordinate = tileCoordinate;
    }

    /**
     *
     * @param tileCoordinate - tile's coordinate
     * @param piece - piece, that will stand on said tile
     * @return if piece exists, returns Occupied Tile, if not, returns an Empty Tile
     */
    public static Tile createTile(int tileCoordinate, Piece piece) {
        if (piece != null){
            return new OccupiedTile(tileCoordinate, piece);
        }
        return emptyTiles.get(tileCoordinate);
    }

    public abstract boolean isTileOccupied();

    public abstract Piece getPiece();

    public int getTileCoordinate(){
        return this.tileCoordinate;
    }

    /**
     * Tile without a piece on it
     */
    public static final class EmptyTile extends Tile{
        EmptyTile(final int coordinate){
            super(coordinate);
        }

        @Override
        public boolean isTileOccupied(){
            return false;
        }

        @Override
        public Piece getPiece(){
            return null;
        }
    }

    /**
     * Tile with a piece on it
     */
    public static final class OccupiedTile extends Tile{
        private final Piece pieceOnTile;

        OccupiedTile(int coordinate, Piece pieceOnTile){
            super(coordinate);
            this.pieceOnTile = pieceOnTile;
        }
        @Override
        public boolean isTileOccupied(){
            return true;
        }

        @Override
        public Piece getPiece(){
            return pieceOnTile;
        }
    }
}
