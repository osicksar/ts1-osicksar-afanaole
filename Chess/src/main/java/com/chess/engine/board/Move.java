package com.chess.engine.board;

import com.chess.engine.pieces.Pawn;
import com.chess.engine.pieces.Piece;
import com.chess.engine.pieces.Rook;


public abstract class Move {
    Board board;
    Piece movedPiece;
    int destinationTile;
    public static Move INVALID_MOVE = new InvalidMove();

    /**
     * Move - constructor for all moves
     * @param board on which the game is played
     * @param movedPiece - piece which is moved somewhere
     * @param destinationTile - tile, where the piece goes
     */
    Move(Board board, Piece movedPiece, int destinationTile){
        this.board = board;
        this.movedPiece = movedPiece;
        this.destinationTile = destinationTile;
    }

    public int getCurrentCoordinate(){
        return this.movedPiece.getPosition();
    }

    /**
     *
     * @return hashCode for comparing Moves
     */
    @Override
    public int hashCode(){
        int result = 1;
        result = 31 * result + this.destinationTile;
        result = 31 * result + this.movedPiece.hashCode();
        return result;
    }

    /**
     *
     * @param isEqual - second parameter
     * @return if Moves are equal - true, if not - false
     */
    @Override
    public boolean equals(Object isEqual){
        if(this == isEqual){
            return true;
        }
        if(!(isEqual instanceof Move)){
            return false;
        }
        Move otherMove = (Move) isEqual;
        return this.getDestinationTile() == otherMove.getDestinationTile() && this.getMovedPiece().equals(otherMove.movedPiece);
    }

    public int getDestinationTile() {
        return destinationTile;
    }

    public Piece getMovedPiece() {
        return movedPiece;
    }

    public boolean isAttack(){
        return false;
    }

    public boolean isCastling(){
        return false;
    }

    public Piece getAttackedPiece(){
        return null;
    }

    /**
     *
     * @return a new Board with all Pieces set up the same, besides the one that moved
     */
    public Board execute() {
        Board.Builder builder = new Board.Builder();
        for(Piece piece:this.board.currentPlayer().getActivePieces()){
            if(!this.movedPiece.equals(piece)){
                builder.setPiece(piece);
            }
        }
        for(Piece piece:this.board.currentPlayer().getOpponent().getActivePieces()){
            builder.setPiece(piece);
        }
        builder.setPiece(this.movedPiece.movePiece(this));
        builder.setPlayer(this.board.currentPlayer().getOpponent().getColour());
        return builder.build();
    }

    /**
     * Move that just moves the piece
     */
    public static class NormalMove extends Move{
        public NormalMove(Board board, Piece movedPiece, int destinationTile){
            super(board, movedPiece, destinationTile);
        }
    }

    /**
     * Move that attacks other piece
     */
    public static class AttackingMove extends Move{
        Piece attackedPiece;
        public AttackingMove(Board board, Piece movedPiece, int destinationTile, Piece attackedPiece){
            super(board, movedPiece, destinationTile);
            this.attackedPiece = attackedPiece;
        }
        @Override
        public int hashCode(){
            return this.attackedPiece.hashCode() + super.hashCode();
        }


        @Override
        public boolean isAttack(){
            return true;
        }

        @Override
        public Piece getAttackedPiece(){
            return this.attackedPiece;
        }
    }

    /**
     * Pawn attacking move
     */
    public static class PawnAttackMove extends AttackingMove{
        Piece attackedPiece;
        public PawnAttackMove(Board board, Piece movedPiece, int destinationTile, Piece attackedPiece){
            super(board, movedPiece, destinationTile, attackedPiece);
        }
    }

    /**
     * Normal Pawn move
     */
    public static class PawnMove extends NormalMove{
        public PawnMove(Board board, Piece movedPiece, int destinationTile){
            super(board, movedPiece, destinationTile);
        }
    }
    public static class PawnEnPassant extends PawnAttackMove{
        Piece attackedPiece;
        public PawnEnPassant(Board board, Piece movedPiece, int destinationTile, Piece attackedPiece){
            super(board, movedPiece, destinationTile, attackedPiece);
            this.attackedPiece = attackedPiece;
        }
        @Override
        public Board execute() {
            Board.Builder builder = new Board.Builder();
            for(Piece piece:this.board.currentPlayer().getActivePieces()){
                if(!this.movedPiece.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for(Piece piece:this.board.currentPlayer().getOpponent().getActivePieces()){
                if(!piece.equals(this.attackedPiece)){
                    builder.setPiece(piece);
                }
            }
            builder.setPiece(this.movedPiece.movePiece(this));
            builder.setPlayer(this.board.currentPlayer().getOpponent().getColour());
            return builder.build();
        }
    }

    /**
     * Pawn moves 2 squares, gets en passant set
     */
    public static class PawnPush extends PawnMove{
        public PawnPush(Board board, Piece movedPiece, int destinationTile){
            super(board, movedPiece, destinationTile);
        }
        @Override
        public Board execute() {
            Board.Builder builder = new Board.Builder();
            for(Piece piece:this.board.currentPlayer().getActivePieces()){
                if(!this.movedPiece.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for(Piece piece:this.board.currentPlayer().getOpponent().getActivePieces()){
                builder.setPiece(piece);
            }
            Pawn movedPawn = (Pawn)this.movedPiece.movePiece(this);
            builder.setPiece(this.movedPiece.movePiece(this));
            builder.setEnPassant(movedPawn);
            builder.setPlayer(this.board.currentPlayer().getOpponent().getColour());
            return builder.build();
        }
    }

    /**
     * Castling move
     */
    static abstract class CastleMove extends Move{
        protected Rook castleRook;
        protected int castleRookStart;
        protected int castleRookDestination;
        public CastleMove(Board board, Piece movedPiece, int destinationTile, Rook castleRook, int castleRookStart, int castleRookDestination){
            super(board, movedPiece, destinationTile);
            this.castleRook = castleRook;
            this.castleRookStart = castleRookStart;
            this.castleRookDestination = castleRookDestination;
        }

        public Rook getCastleRook() {
            return this.castleRook;
        }
        @Override
        public boolean isCastling(){
            return true;
        }
        @Override
        public Board execute(){
            Board.Builder builder = new Board.Builder();
            for(Piece piece:this.board.currentPlayer().getActivePieces()){
                if(!this.movedPiece.equals(piece) && !this.castleRook.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for(Piece piece:this.board.currentPlayer().getOpponent().getActivePieces()){
                builder.setPiece(piece);
            }
            builder.setPiece(this.movedPiece.movePiece(this));
            builder.setPiece(new Rook(this.castleRook.getColour(), this.castleRookDestination, false));
            builder.setPlayer(this.board.currentPlayer().getOpponent().getColour());
            return builder.build();
        }
    }

    /**
     * Castling from King Side
     */
    public static class KingSideCastleMove extends CastleMove{
        public KingSideCastleMove(Board board, Piece movedPiece, int destinationTile, Rook castleRook, int castleRookStart, int castleRookDestination){
            super(board, movedPiece, destinationTile, castleRook, castleRookStart, castleRookDestination);
        }
        @Override
        public String toString(){
            return "O-O";
        }
    }

    /**
     * Castling from Queen Side
     */
    public static class QueenSideCastleMove extends CastleMove{
        public QueenSideCastleMove(Board board, Piece movedPiece, int destinationTile, Rook castleRook, int castleRookStart, int castleRookDestination){
            super(board, movedPiece, destinationTile, castleRook, castleRookStart, castleRookDestination);
        }
        @Override
        public String toString(){
            return "O-O-O";
        }
    }

    /**
     * An Invalid move
     */
    public static class InvalidMove extends Move{
        public InvalidMove(){
            super(null, null, -1);
        }
        @Override
        public Board execute() {
            throw new RuntimeException("Cannot execute illegal move");
        }
    }

    /**
     * Creates a move
     */
    public static class MoveFactory{
        private MoveFactory(){
        }
        public static Move createMove(Board board, int currentCoordinate, int destinationTile){
            for(Move move: board.getAllLegalMoves()){
                if(move.getCurrentCoordinate() == currentCoordinate && move.getDestinationTile() == destinationTile){
                    return move;
                }
            }
            return INVALID_MOVE;
        }
    }
}
