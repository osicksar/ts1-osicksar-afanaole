package com.chess.engine.player;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import com.chess.engine.pieces.Piece;
import com.chess.engine.pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Player, that controls black pieces
 */
public class BlackPlayer extends Player{
    /**
     *
     * @param board - Board, on which the game is played
     * @param baseWhiteLegalMoves - moves of white player
     * @param baseBlackLegalMoves - moves of black player
     */
    public BlackPlayer(Board board, Collection<Move> baseWhiteLegalMoves, Collection<Move> baseBlackLegalMoves) {
        super(board, baseBlackLegalMoves, baseWhiteLegalMoves);
    }

    @Override
    public Collection<Piece> getActivePieces() {
        return this.board.getBlackPieces();
    }

    @Override
    public Colour getColour() {
        return Colour.black;
    }

    @Override
    public Player getOpponent() {
        return this.board.whitePlayer();
    }

    /**
     *
     * @param playerMoves - black player's moves
     * @param opponentMoves - white player's moves
     * @return  a list of possible castling moves, if exist
     */
    @Override
    public Collection<Move> calculateKingCastles(Collection<Move> playerMoves, Collection<Move> opponentMoves) {
        List<Move> kingCastles = new ArrayList<>();
        if(this.playerKing.isFirstMove() && !this.isInCheck()){
            //Black king side castle
            if(!this.board.getTile(5).isTileOccupied() &&
                    !this.board.getTile(6).isTileOccupied() &&
                    Player.calculateAttacksOnTile(5, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(6, opponentMoves).isEmpty()){
                Tile rookTile = this.board.getTile(7);
                if(rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() && rookTile.getPiece().getPieceType().isRook()){
                    kingCastles.add(new Move.KingSideCastleMove(this.board, this.playerKing, 6, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 5));
                }
            }
            //Black queen side castle
            if(!this.board.getTile(1).isTileOccupied() &&
                    !this.board.getTile(2).isTileOccupied() &&
                    !this.board.getTile(3).isTileOccupied() &&
                    Player.calculateAttacksOnTile(1, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(2, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(3, opponentMoves).isEmpty()){
                Tile rookTile = this.board.getTile(0);
                if(rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() && rookTile.getPiece().getPieceType().isRook()){
                    kingCastles.add(new Move.QueenSideCastleMove(this.board, this.playerKing, 2, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 3));
                }
            }
        }
        return kingCastles;
    }
}
