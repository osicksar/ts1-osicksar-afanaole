package com.chess.engine.player;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.pieces.King;
import com.chess.engine.pieces.Piece;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Someone, who plays the game
 */
public abstract class Player {
    protected Board board;
    protected King playerKing;
    protected Collection<Move> legalMoves;
    private boolean isInCheck;

    /**
     *
     * @param board - Board, on which the game is played
     * @param legalMoves - list of this player's moves
     * @param opponentMoves - list of opponent's moves
     */
    Player(Board board, Collection<Move> legalMoves, Collection<Move> opponentMoves){
        this.board = board;
        this.playerKing = establishKing();
        this.legalMoves = legalMoves;
        this.legalMoves.addAll(calculateKingCastles(legalMoves, opponentMoves));
        this.isInCheck = !Player.calculateAttacksOnTile(this.getPlayerKing().getPosition(), opponentMoves).isEmpty();
    }

    /**
     *
     * @param piecePosition - position of a Piece
     * @param opponentMoves - list of opponent's moves
     * @return list of attacking moves, that attack selected piece
     */
    protected static Collection<Move> calculateAttacksOnTile(int piecePosition, Collection<Move> opponentMoves){
        List<Move> attackMoves = new ArrayList<>();
        for(Move move: opponentMoves){
            if(piecePosition == move.getDestinationTile()){
                attackMoves.add(move);
            }
        }
        return attackMoves;
    }

    /**
     *
     * @return starts the game only when the King exists
     */
    private King establishKing() {
        for(Piece piece : getActivePieces()) {
            if (piece.getPieceType().isKing()) {
                return (King) piece;
            }
        }
        throw new RuntimeException("No King on Board!");
    }

    public King getPlayerKing() {
        return this.playerKing;
    }

    public Collection<Move> getLegalMoves() {
        return this.legalMoves;
    }

    public boolean isMoveLegal(Move move){
        return this.legalMoves.contains(move);
    }
    public boolean isInCheck(){
        return this.isInCheck;
    }
    public boolean isInMate(){
        return this.isInCheck && !hasEscapeMoves();
    }

    /**
     *
     * @return if piece can escape from a being attacked
     */
    private boolean hasEscapeMoves() {
        for(Move move: this.legalMoves){
            MoveTransition transition = makeMove(move);
            if(transition.getMoveStatus().isDone()){
                return true;
            }
        }
        return false;
    }

    public boolean isInStaleMate(){
        return !this.isInCheck && !hasEscapeMoves();
    }

    /**
     * Creates an imaginary board with complete move, and checks, if position is legal
     * @param move - Move which is about to happen
     * @return Move Transition, which holds imaginary board and previous move's status
     */
    public MoveTransition makeMove(Move move){
        if(!isMoveLegal(move)){
            return new MoveTransition(this.board, move, MoveStatus.ILLEGAL);
        }
        Board transitionBoard = move.execute();
        Collection<Move> kingAttacks = Player.calculateAttacksOnTile(transitionBoard.currentPlayer().getOpponent().getPlayerKing().getPosition(), transitionBoard.currentPlayer().getLegalMoves());
        if(!kingAttacks.isEmpty()){
            return new MoveTransition(this.board, move, MoveStatus.LEAVES_PLAYER_IN_CHECK);
        }
        return new MoveTransition(transitionBoard, move, MoveStatus.DONE);
    }


    public abstract Collection<Piece> getActivePieces();
    public abstract Colour getColour();
    public abstract Player getOpponent();
    public abstract Collection<Move> calculateKingCastles(Collection<Move> playerMoves, Collection<Move> opponentMoves);
}
