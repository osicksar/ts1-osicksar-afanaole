package com.chess.engine.player;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import com.chess.engine.pieces.Piece;
import com.chess.engine.pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Player, that controls white pieces
 */
public class WhitePlayer extends Player{
    /**
     *
     * @param board -  Board, on which the game is played
     * @param baseWhiteLegalMoves - possible moves of a white player
     * @param baseBlackLegalMoves - possible moves of a black player
     */
    public WhitePlayer(Board board, Collection<Move> baseWhiteLegalMoves, Collection<Move> baseBlackLegalMoves) {
        super(board, baseWhiteLegalMoves, baseBlackLegalMoves);
    }

    @Override
    public Collection<Piece> getActivePieces() {
        return this.board.getWhitePieces();
    }

    @Override
    public Colour getColour() {
        return Colour.white;
    }

    @Override
    public Player getOpponent() {
        return this.board.blackPlayer();
    }

    /**
     *
     * @param playerMoves - white player's moves
     * @param opponentMoves - black player's moves
     * @return  a list of possible castling moves, if exist
     */
    @Override
    public Collection<Move> calculateKingCastles(Collection<Move> playerMoves, Collection<Move> opponentMoves) {
        List<Move> kingCastles = new ArrayList<>();
        if(this.playerKing.isFirstMove() && !this.isInCheck()){
            //White king side castle
            if(!this.board.getTile(61).isTileOccupied() &&
                    !this.board.getTile(62).isTileOccupied() &&
                    Player.calculateAttacksOnTile(61, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(62, opponentMoves).isEmpty()){
                Tile rookTile = this.board.getTile(63);
                if(rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() && rookTile.getPiece().getPieceType().isRook()){
                    kingCastles.add(new Move.KingSideCastleMove(this.board, this.playerKing, 62, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 61));
                }
            }
            //White queen side castle
            if(!this.board.getTile(57).isTileOccupied() &&
                    !this.board.getTile(58).isTileOccupied() &&
                    !this.board.getTile(59).isTileOccupied() &&
                    Player.calculateAttacksOnTile(57, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(58, opponentMoves).isEmpty() &&
                    Player.calculateAttacksOnTile(59, opponentMoves).isEmpty()){
                Tile rookTile = this.board.getTile(56);
                if(rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove() && rookTile.getPiece().getPieceType().isRook()){
                    kingCastles.add(new Move.QueenSideCastleMove(this.board, this.playerKing, 58, (Rook) rookTile.getPiece(), rookTile.getTileCoordinate(), 59));
                }
            }
        }
        return kingCastles;
    }
}
