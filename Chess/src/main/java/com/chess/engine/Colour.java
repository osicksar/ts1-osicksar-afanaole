package com.chess.engine;

import com.chess.engine.player.BlackPlayer;
import com.chess.engine.player.Player;
import com.chess.engine.player.WhitePlayer;

public enum Colour {
	white{
	    @Override
        public boolean isBlack(){
	        return false;
        }
        @Override
        public boolean isWhite(){
            return true;
        }
        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer){
            return whitePlayer;
        }
    },
    black{
        @Override
        public boolean isBlack(){
            return false;
        }
        @Override
        public boolean isWhite(){
            return true;
        }
        @Override
        public Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer){
            return blackPlayer;
        }
    };

	public abstract boolean isBlack();
	public abstract boolean isWhite();
    public abstract Player choosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer);
}
