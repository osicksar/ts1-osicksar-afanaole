package com.chess.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame{
	private JButton playerVsPlayerButton;
	public JPanel panelMain;
	private JButton playerVsComputerButton;
	private JButton savedGameButton;
	private JButton statisticButton;



	public Menu() {

		JFrame frame = new JFrame("Menu");
		frame.setContentPane(panelMain);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(800, 800);

		playerVsPlayerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				JPanel panel = new settingGame().panelMain;
				frame.setContentPane(panel);
				frame.pack();
				frame.setVisible(true);
				frame.setSize(800, 800);
			}
		});
		playerVsComputerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {


				JPanel panel = new settingGame().panelMain;
				frame.setContentPane(panel);
				frame.pack();
				frame.setVisible(true);
				frame.setSize(800, 800);
				


			}
		});
		savedGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		statisticButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
	}


	public static void main(String[] args) {
		new Menu();
	}
}
