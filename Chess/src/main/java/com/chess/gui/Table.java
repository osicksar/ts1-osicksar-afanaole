package com.chess.gui;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import com.chess.engine.pieces.Piece;
import com.chess.engine.player.MoveTransition;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

public class Table {
    public static Colour winner;
    private final Frame gameFrame;
    private final BoardPanel boardPanel;
    private Board chessBoard;
    private Tile sourceTile;
    private Tile destinationTile;
    private Piece humanMovedPiece;

    String pieceIconPath = "pieceIcons/";

    private static Dimension gameResolution = new Dimension(800, 800);
    private static Dimension tileResolution = new Dimension(100, 100);

    /**
     * Chessboard GUI
     */
    public Table(){
        this.gameFrame = new Frame("Chess");
        this.gameFrame.setLayout(new BorderLayout());
        this.gameFrame.setSize(gameResolution);
        this.chessBoard = Board.createStandardBoard();
        this.boardPanel = new BoardPanel();
        this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);
        this.gameFrame.setVisible(true);
    }

    private class BoardPanel extends Panel {
        final List<TilePanel> boardTiles;

        BoardPanel() {
            super(new GridLayout(8,8));
            this.boardTiles = new ArrayList<>();
            for(int i = 0; i < 64; i++){
                final TilePanel tilePanel = new TilePanel(this, i);
                this.boardTiles.add(tilePanel);
                add(tilePanel);
            }
            setPreferredSize(gameResolution);
            validate();

        }

        /**
         * Draws a board
         * @param board - board, on which the game is played
         */
        public void drawBoard(Board board) {
            removeAll();
            for(TilePanel tilePanel: boardTiles){
                tilePanel.drawTile(board);
                add(tilePanel);
            }
            if(chessBoard.currentPlayer().isInMate()){
                if(chessBoard.currentPlayer().getColour() != Colour.black) {
                    JFrame frame = new JFrame("End Game");
                    JPanel panel = new blackWon().panelMain;
                    JLabel label = new JLabel();
                    frame.setContentPane(panel);
                    frame.pack();
                    frame.setVisible(true);
                    frame.setSize(800, 800);
                }
                else{
                    JFrame frame = new JFrame("End Game");
                    JPanel panel = new whiteWon().panelMain;
                    JLabel label = new JLabel();
                    frame.setContentPane(panel);
                    frame.pack();
                    frame.setVisible(true);
                    frame.setSize(800, 800);
                }
            }
            validate();
            repaint();
        }
    }

    private class TilePanel extends Panel{
        private final int tileId;

        TilePanel(final BoardPanel boardPanel, final int tileId){
            super(new GridBagLayout());
            this.tileId = tileId;
            setPreferredSize(tileResolution);
            assignTileColour();
            assignTilePieceIcon(chessBoard);
            addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if(isRightMouseButton(e)){
                        //Deselect a piece
                        sourceTile = null;
                        destinationTile = null;
                        humanMovedPiece = null;
                    }
                    else if(isLeftMouseButton(e)){
                        //First click selects, second moves
                        if(sourceTile == null){
                                sourceTile = chessBoard.getTile(tileId);
                                humanMovedPiece = sourceTile.getPiece();
                                if (humanMovedPiece == null) {
                                    sourceTile = null;
                                }
                        }else{
                            destinationTile = chessBoard.getTile(tileId);
                            Move move = Move.MoveFactory.createMove(chessBoard, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
                            MoveTransition transition = chessBoard.currentPlayer().makeMove(move);
                            if (transition.getMoveStatus().isDone()){
                                chessBoard = transition.getTransitionBoard();
                            }
                            sourceTile = null;
                            destinationTile = null;
                            humanMovedPiece = null;
                        }
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                boardPanel.drawBoard(chessBoard);
                            }
                        });
                    }
                }

                @Override
                public void mousePressed(final MouseEvent e) {

                }

                @Override
                public void mouseReleased(final MouseEvent e) {

                }

                @Override
                public void mouseEntered(final MouseEvent e) {

                }

                @Override
                public void mouseExited(final MouseEvent e) {

                }
            });
            validate();
        }
        private void highlightLegalMoves(Board board){
            if(true){
                for(Move move: pieceLegalMoves(board)){
                    if(move.getDestinationTile() == this.tileId){
                        try{
                            add(new JLabel(new ImageIcon(ImageIO.read(new File("pieceIcons/green_dot.png")))));
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        private Collection<Move> pieceLegalMoves(Board board) {
            if(humanMovedPiece !=null && humanMovedPiece.getColour() == board.currentPlayer().getColour()){
                return humanMovedPiece.getPossibleMoves(board);
            }
            return Collections.emptyList();
        }

        /**
         * Assigns an icon to a tile
         * @param board - board, on which the game is played
         */
        public void assignTilePieceIcon(Board board){
            this.removeAll();
            if(board.getTile(this.tileId).isTileOccupied()){
                try {
                    BufferedImage image = ImageIO.read(new File(pieceIconPath +
                            board.getTile(this.tileId).getPiece().getColour().toString().substring(0,1) +
                            board.getTile(this.tileId).getPiece().toString() +
                            ".png"));
                    add(new JLabel(new ImageIcon(image)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void assignTileColour() {
            boolean isLight = ((tileId + tileId / 8) % 2 == 0);
            setBackground(isLight ? Color.white : Color.gray);
        }

        /**
         * Draws tile on board
         * @param board - board, on which the game is played
         */
        public void drawTile(Board board) {
            assignTileColour();
            assignTilePieceIcon(board);
            highlightLegalMoves(chessBoard);
            validate();
            repaint();
        }

    }
}
