package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CheckMateTest {
    @Test
    public void isInMate_setKingsBishopAndARook_isInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Bishop testBishop = new Bishop(Colour.white, 22, true);
        Rook testRook = new Rook(Colour.white, 63, true);
        King testKingWhite = new King(Colour.white, 5, true);
        King testKingBlack = new King(Colour.black, 7, true);
        builder.setPiece(testBishop);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInMate());
    }
    @Test
    public void isInMate_setKingsAndAQueen_isInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Queen testQueen = new Queen(Colour.white, 22, true);
        King testKingWhite = new King(Colour.white, 21, true);
        King testKingBlack = new King(Colour.black, 23, true);
        builder.setPiece(testQueen);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInMate());
    }
    @Test
    public void isInMate_setKingsAndARook_notInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Rook testRook = new Rook(Colour.white, 16, true);
        King testKingWhite = new King(Colour.white, 29, true);
        King testKingBlack = new King(Colour.black, 0, true);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertFalse(board.blackPlayer().isInMate());
    }
    @Test
    public void isInMate_setKingsPawnAndARook_isInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Rook testRook = new Rook(Colour.white, 0, true);
        Pawn testPawn = new Pawn(Colour.white, 11, true);
        King testKingWhite = new King(Colour.white, 19, true);
        King testKingBlack = new King(Colour.black, 3, true);
        builder.setPiece(testRook);
        builder.setPiece(testPawn);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInMate());
    }
    @Test
    public void isInMate_setKingsAndAPawn_notInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Pawn testPawn = new Pawn(Colour.white, 11, true);
        King testKingWhite = new King(Colour.white, 19, true);
        King testKingBlack = new King(Colour.black, 3, true);
        builder.setPiece(testPawn);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertFalse(board.blackPlayer().isInMate());
    }
}
