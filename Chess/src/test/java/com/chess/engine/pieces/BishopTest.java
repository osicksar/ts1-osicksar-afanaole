package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import static com.chess.engine.board.Board.createChessBoard;
import static org.junit.jupiter.api.Assertions.*;


public class BishopTest {
    @Test
    public void BishopGetPossibleMoves_setBishopAt26_GetMoves(){
        //arrange
        Collection<Move> bishopMoves = new ArrayList<>();
        Collection<Move> bishopExceptedMoves = new ArrayList<>();

        Board.Builder builder = new Board.Builder();

        Bishop testBishop = new Bishop(Colour.white, 26, true);
        Rook testRook = new Rook(Colour.black, 33, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 3, true);

        builder.setPiece(testBishop);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);

        builder.setPlayer(Colour.white);
        Board board = builder.build();
        createChessBoard(builder);
        //act
        bishopMoves = testBishop.getPossibleMoves(board);
        //Move(19, 12, 5, 35, 44, 53, 62, 17, 8, 33)
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 19));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 12));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 5));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 35));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 44));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 53));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 62));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 17));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 8));
        bishopExceptedMoves.add(new Move.AttackingMove(board, testBishop, 33, testRook));
        //assert
        for (Move move: bishopMoves) {
            System.out.println("Destination tile: " + move.getDestinationTile());
        }
        Assertions.assertTrue(bishopExceptedMoves.size() == bishopMoves.size());
    }

    @Test
    public void BishopGetPossibleMoves_setBishopAt24_GetMoves(){
        //arrange
        Collection<Move> bishopMoves = new ArrayList<>();
        Collection<Move> bishopExceptedMoves = new ArrayList<>();

        Board.Builder builder = new Board.Builder();

        Bishop testBishop = new Bishop(Colour.white, 24, true);
        Rook testRook = new Rook(Colour.white, 51, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 3, true);


        builder.setPiece(testBishop);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);

        builder.setPlayer(Colour.white);
        Board board = builder.build();
        createChessBoard(builder);

        //act
        bishopMoves = testBishop.getPossibleMoves(board);
        //Move(17, 10, 3, 33, 42)
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 17));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 10));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 3));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 33));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 42));

        //assert
        for (Move move: bishopMoves) {
            System.out.println("Destination tile: " + move.getDestinationTile());
        }
        Assertions.assertTrue(bishopExceptedMoves.size() == bishopMoves.size());
    }

    @Test
    public void BishopGetPossibleMoves_setBishopAt31_GetMoves(){
        //arrange
        Collection<Move> bishopMoves = new ArrayList<>();
        Collection<Move> bishopExceptedMoves = new ArrayList<>();

        Board.Builder builder = new Board.Builder();

        Bishop testBishop = new Bishop(Colour.black, 31, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 3, true);

        builder.setPiece(testBishop);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);

        builder.setPlayer(Colour.black);
        Board board = builder.build();

        //act
        bishopMoves = testBishop.getPossibleMoves(board);
        //Move(22, 13, 4, 38, 45, 52, 59)
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 22));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 13));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 4));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 38));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 45));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 52));
        bishopExceptedMoves.add(new Move.NormalMove(board, testBishop, 59));

        //assert
        for (Move move: bishopMoves) {
            System.out.println("Destination tile: " + move.getDestinationTile());
        }
        Assertions.assertTrue(bishopExceptedMoves.size() == bishopMoves.size());
    }
}
