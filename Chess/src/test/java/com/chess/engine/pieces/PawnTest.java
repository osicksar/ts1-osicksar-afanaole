package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import static com.chess.engine.board.Board.createChessBoard;
import static org.junit.jupiter.api.Assertions.*;

public class PawnTest {
	@Test
	public void PawnGetPossibleMoves_setPawnAt10_GetMoves(){
		//arrange
		Collection<Move> pawnMoves = new ArrayList<>();
		Collection<Move> pawnExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Pawn testPawn = new Pawn(Colour.black, 10, true);
		Rook testRook = new Rook(Colour.white, 17, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);


		builder.setPiece(testPawn);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.black);
		Board board = builder.build();
		createChessBoard(builder);
		//act
		pawnMoves = testPawn.getPossibleMoves(board);
		//Move(34, 33)
		pawnExceptedMoves.add(new Move.NormalMove(board, testPawn, 18));
		pawnExceptedMoves.add(new Move.NormalMove(board, testPawn, 26));
		pawnExceptedMoves.add(new Move.AttackingMove(board, testPawn, 17, testRook));
		//assert
		for (Move move: pawnMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(pawnExceptedMoves.size() == pawnMoves.size());
	}

	@Test
	public void PawnGetPossibleMoves_setPawnAt11_GetMoves(){
		//arrange
		Collection<Move> pawnMoves = new ArrayList<>();
		Collection<Move> pawnExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Pawn testPawn = new Pawn(Colour.black, 11, true);
		Rook testRook = new Rook(Colour.black, 27, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);


		builder.setPiece(testPawn);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.black);
		Board board = builder.build();

		//act
		pawnMoves = testPawn.getPossibleMoves(board);
		//Move(19)
		pawnExceptedMoves.add(new Move.NormalMove(board, testPawn, 19));

		//assert
		for (Move move: pawnMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(pawnExceptedMoves.size() == pawnMoves.size());
	}

	@Test
	public void PawnGetPossibleMoves_setPawnAt48_GetMoves(){
		//arrange
		Collection<Move> pawnMoves = new ArrayList<>();
		Collection<Move> pawnExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Pawn testPawn = new Pawn(Colour.white, 48, true);
		Rook testRook = new Rook(Colour.black, 39, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);


		builder.setPiece(testPawn);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		pawnMoves = testPawn.getPossibleMoves(board);
		//Move(40, 32)
		pawnExceptedMoves.add(new Move.NormalMove(board, testPawn, 40));
		pawnExceptedMoves.add(new Move.NormalMove(board, testPawn, 32));

		//assert
		for (Move move: pawnMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(pawnExceptedMoves.size() == pawnMoves.size());
	}
}
