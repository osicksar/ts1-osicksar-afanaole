package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

public class CheckTest {
    @Test
    public void isInCheck_setKingsAndABishop_isInCheck(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Bishop testBishop = new Bishop(Colour.white, 17, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 3, true);
        builder.setPiece(testBishop);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInCheck());
    }
    @Test
    public void isInCheck_setKingsAndAQueen_notInCheck(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Queen testQueen = new Queen(Colour.white, 49, true);
        King testKingWhite = new King(Colour.white, 42, true);
        King testKingBlack = new King(Colour.black, 7, true);
        builder.setPiece(testQueen);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInCheck());
    }
    @Test
    public void isInCheck_setKingsAndARook_isInCheck(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Rook testRook = new Rook(Colour.white, 0, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 3, true);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInCheck());
    }
    @Test
    public void isInCheck_setKingsAndAKnight_notInCheck(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Knight testKnight = new Knight(Colour.white, 13, true);
        King testKingWhite = new King(Colour.white, 61, true);
        King testKingBlack = new King(Colour.black, 6, true);
        builder.setPiece(testKnight);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertFalse(board.blackPlayer().isInCheck());
    }
    @Test
    public void isInCheck_setKingsAndAQueen_isInCheckMate(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Queen testQueen = new Queen(Colour.white, 14, true);
        King testKingWhite = new King(Colour.white, 21, true);
        King testKingBlack = new King(Colour.black, 7, true);
        builder.setPiece(testQueen);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.black);
        Board board = builder.build();
        //act & assert
        Assertions.assertTrue(board.blackPlayer().isInCheck());
    }
}
