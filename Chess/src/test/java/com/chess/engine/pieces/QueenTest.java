package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;


import static com.chess.engine.board.Board.createChessBoard;
import static org.junit.jupiter.api.Assertions.*;

public class QueenTest {
	@Test
	public void QueenGetPossibleMoves_setQueenAt26_GetMoves(){
		//arrange
		Collection<Move> queenMoves = new ArrayList<>();
		Collection<Move> queenExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Queen testQueen = new Queen(Colour.white, 26, true);
		Rook testRook = new Rook(Colour.black, 44, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testQueen);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();
		createChessBoard(builder);
		//act
		queenMoves = testQueen.getPossibleMoves(board);
		//Move(19, 12, 5, 27, 28, 29, 30, 31, 35, 44, 18, 10, 2, 34, 42, 50, 58, 25, 24, 17, 8, 33, 40)
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 19));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 12));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 5));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 27));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 28));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 29));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 30));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 31));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 35));
		queenExceptedMoves.add(new Move.AttackingMove(board, testQueen, 44, testRook));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 18));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 10));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 2));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 34));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 42));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 50));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 58));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 25));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 24));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 17));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 8));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 33));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 40));


		//assert
		for (Move move: queenMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(queenExceptedMoves.size() == queenMoves.size());
	}

	@Test
	public void QueenGetPossibleMoves_setQueenAt24_GetMoves(){
		//arrange
		Collection<Move> queenMoves = new ArrayList<>();
		Collection<Move> queenExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Queen testQueen = new Queen(Colour.white, 24, true);
		Rook testRook = new Rook(Colour.white, 27, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testQueen);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		queenMoves = testQueen.getPossibleMoves(board);
		//Move(17, 10, 3, 25, 26, 33, 42, 51, 60, 16, 8, 0, 32, 40, 48, 56)
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 17));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 10));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 3));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 25));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 26));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 33));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 42));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 51));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 60));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 16));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 8));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 0));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 32));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 40));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 48));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 56));


		//assert
		for (Move move: queenMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(queenExceptedMoves.size() == queenMoves.size());
	}

	@Test
	public void QueenGetPossibleMoves_setQueenAt31_GetMoves(){
		//arrange
		Collection<Move> queenMoves = new ArrayList<>();
		Collection<Move> queenExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Queen testQueen = new Queen(Colour.black, 31, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testQueen);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.black);
		Board board = builder.build();

		//act
		queenMoves = testQueen.getPossibleMoves(board);
		//Move(23, 15, 7, 39, 47, 55, 63, 30, 29, 28, 27, 26, 25, 24, 22, 13, 4, 38, 45, 52, 59)
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 23));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 15));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 7));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 39));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 47));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 55));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 63));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 30));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 29));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 28));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 27));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 26));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 25));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 24));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 22));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 13));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 4));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 38));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 45));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 52));
		queenExceptedMoves.add(new Move.NormalMove(board, testQueen, 59));

		//assert
		for (Move move: queenMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(queenExceptedMoves.size() == queenMoves.size());
	}
}
