package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import static com.chess.engine.board.Board.createChessBoard;
import static org.junit.jupiter.api.Assertions.*;


public class RookTest {
	@Test
	public void RookGetPossibleMoves_setRookAt26_GetMoves(){
		//arrange
		Collection<Move> rookMoves = new ArrayList<>();
		Collection<Move> rookExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Rook testRook = new Rook(Colour.white, 26, true);
		Knight testKnight = new Knight(Colour.black, 29, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testRook);
		builder.setPiece(testKnight);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		rookMoves = testRook.getPossibleMoves(board);
		//Move(27, 28, 29, 18, 10, 2, 34, 42, 50, 58, 25, 24)
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 27));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 28));
		rookExceptedMoves.add(new Move.AttackingMove(board, testRook, 29, testKnight));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 18));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 10));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 2));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 34));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 42));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 50));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 58));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 25));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 24));
		//assert
		for (Move move: rookMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(rookExceptedMoves.size() == rookMoves.size());
	}

	@Test
	public void RookGetPossibleMoves_setRookAt24_GetMoves(){
		//arrange
		Collection<Move> rookMoves = new ArrayList<>();
		Collection<Move> rookExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Rook testRook = new Rook(Colour.white, 24, true);
		Knight testKnight = new Knight(Colour.white, 27, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);


		builder.setPiece(testRook);
		builder.setPiece(testKnight);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		rookMoves = testRook.getPossibleMoves(board);
		//Move(25, 26, 16, 8, 0, 32, 40, 48, 56)
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 25));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 26));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 16));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 8));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 0));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 32));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 40));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 48));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 56));

		//assert
		for (Move move: rookMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(rookExceptedMoves.size() == rookMoves.size());

	}

	@Test
	public void RookGetPossibleMoves_setRookAt31_GetMoves(){
		//arrange
		Collection<Move> rookMoves = new ArrayList<>();
		Collection<Move> rookExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Rook testRook = new Rook(Colour.black, 31, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.black);
		Board board = builder.build();

		//act
		rookMoves = testRook.getPossibleMoves(board);
		//Move(23, 15, 7, 39, 47, 55, 63, 30, 29, 28, 27, 26, 25, 24)
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 23));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 15));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 7));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 39));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 47));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 55));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 63));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 30));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 29));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 28));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 27));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 26));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 25));
		rookExceptedMoves.add(new Move.NormalMove(board, testRook, 24));

		//assert
		for (Move move: rookMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(rookExceptedMoves.size() == rookMoves.size());
	}
}
