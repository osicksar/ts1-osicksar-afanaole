package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;


import static org.junit.jupiter.api.Assertions.*;


public class KnightTest {

	@Test
	public void KnightGetPossibleMoves_setKnightAt26_GetMoves(){
		//arrange
		Collection<Move> knightMoves = new ArrayList<>();
		Collection<Move> knightExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Knight testKnight = new Knight(Colour.white, 26, true);
		Rook testRook = new Rook(Colour.black, 43, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testKnight);
		builder.setPiece(testRook);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		knightMoves = testKnight.getPossibleMoves(board);
		//Move(20, 36, 11, 43, 9, 41, 16, 32)
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 20));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 36));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 11));
		knightExceptedMoves.add(new Move.AttackingMove(board, testKnight, 43, testRook));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 9));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 41));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 16));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 32));

		//assert
		for (Move move: knightMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(knightExceptedMoves.size() == knightMoves.size());
	}

	@Test
	public void KnightGetPossibleMoves_setKnightAt24_GetMoves(){
		//arrange
		Collection<Move> knightMoves = new ArrayList<>();
		Collection<Move> knightExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Knight testKnight = new Knight(Colour.white, 24, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testKnight);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();

		//act
		knightMoves = testKnight.getPossibleMoves(board);
		//Move(18, 34, 9, 41)
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 18));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 34));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 9));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 41));


		//assert
		for (Move move: knightMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(knightExceptedMoves.size() == knightMoves.size());
	}

	@Test
	public void KnightGetPossibleMoves_setKnightAt31_GetMoves(){
		//arrange
		Collection<Move> knightMoves = new ArrayList<>();
		Collection<Move> knightExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		Knight testKnight = new Knight(Colour.black, 31, true);
		King testKingWhite = new King(Colour.white, 61, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testKnight);
		builder.setPiece(testKingWhite);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.black);
		Board board = builder.build();

		//act
		knightMoves = testKnight.getPossibleMoves(board);
		//Move(14, 46, 21, 37)
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 14));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 46));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 21));
		knightExceptedMoves.add(new Move.NormalMove(board, testKnight, 37));

		//assert
		for (Move move: knightMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(knightExceptedMoves.size() == knightMoves.size());
	}
}

