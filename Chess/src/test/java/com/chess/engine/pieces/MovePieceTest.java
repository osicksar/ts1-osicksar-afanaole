package com.chess.engine.pieces;

import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import com.chess.engine.board.Tile;
import com.chess.engine.player.MoveTransition;
import com.chess.engine.player.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MovePieceTest {
    @Test
    public void movePiece_setKingsAndBishopMoveBishop_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Bishop testBishop = new Bishop(Colour.white, 51, true);
        King testKingWhite = new King(Colour.white, 25, true);
        King testKingBlack = new King(Colour.black, 12, true);
        builder.setPiece(testBishop);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testBishop.getPosition());
        Tile destinationTile = board.getTile(42);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.BISHOP && board.currentPlayer().getColour() == Colour.black);
    }
    @Test
    public void movePiece_setKingsAndQueenMoveQueen_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Queen testQueen = new Queen(Colour.white, 62, true);
        King testKingWhite = new King(Colour.white, 0, true);
        King testKingBlack = new King(Colour.black, 18, true);
        builder.setPiece(testQueen);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testQueen.getPosition());
        Tile destinationTile = board.getTile(6);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.QUEEN && board.currentPlayer().getColour() == Colour.black);
    }
    @Test
    public void movePiece_setKingsAndRookMoveRook_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Rook testRook = new Rook(Colour.white, 58, true);
        King testKingWhite = new King(Colour.white, 25, true);
        King testKingBlack = new King(Colour.black, 12, true);
        builder.setPiece(testRook);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testRook.getPosition());
        Tile destinationTile = board.getTile(26);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.ROOK && board.currentPlayer().getColour() == Colour.black);
    }
    @Test
    public void movePiece_setKingsAndKnightMoveKnight_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Knight testKnight = new Knight(Colour.white, 51, true);
        King testKingWhite = new King(Colour.white, 25, true);
        King testKingBlack = new King(Colour.black, 12, true);
        builder.setPiece(testKnight);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testKnight.getPosition());
        Tile destinationTile = board.getTile(45);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.KNIGHT && board.currentPlayer().getColour() == Colour.black);
    }
    @Test
    public void movePiece_setKingsAndPawnMovePawn_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        Pawn testPawn = new Pawn(Colour.white, 51, true);
        King testKingWhite = new King(Colour.white, 25, true);
        King testKingBlack = new King(Colour.black, 12, true);
        builder.setPiece(testPawn);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testPawn.getPosition());
        Tile destinationTile = board.getTile(35);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.PAWN && board.currentPlayer().getColour() == Colour.black);
    }
    @Test
    public void movePiece_setKingsMoveKing_pieceMoved(){
        //arrange
        Board.Builder builder = new Board.Builder();
        King testKingWhite = new King(Colour.white, 25, true);
        King testKingBlack = new King(Colour.black, 12, true);
        builder.setPiece(testKingWhite);
        builder.setPiece(testKingBlack);
        builder.setPlayer(Colour.white);
        Board board = builder.build();
        //act
        Tile sourceTile = board.getTile(testKingWhite.getPosition());
        Tile destinationTile = board.getTile(17);
        Move move = Move.MoveFactory.createMove(board, sourceTile.getTileCoordinate(), destinationTile.getTileCoordinate());
        MoveTransition transition = board.currentPlayer().makeMove(move);
        if (transition.getMoveStatus().isDone()){
            board = transition.getTransitionBoard();
        }
        //assert
        Assertions.assertTrue(board.getTile(destinationTile.getTileCoordinate()).getPiece().getPieceType() == Piece.PieceType.KING && board.currentPlayer().getColour() == Colour.black);
    }
}
