package com.chess.engine.pieces;
import com.chess.engine.Colour;
import com.chess.engine.board.Board;
import com.chess.engine.board.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;


import static com.chess.engine.board.Board.createChessBoard;

public class KingTest {
	@Test
	public void KingGetPossibleMoves_setKingAt26_GetMoves(){
		//arrange
		Collection<Move> kingMoves = new ArrayList<>();
		Collection<Move> kingExceptedMoves = new ArrayList<>();

		Board.Builder builder = new Board.Builder();

		King testKing = new King(Colour.white, 26, true);
		King testKingBlack = new King(Colour.black, 3, true);

		builder.setPiece(testKing);
		builder.setPiece(testKingBlack);

		builder.setPlayer(Colour.white);
		Board board = builder.build();
		createChessBoard(builder);
		//act
		kingMoves = testKing.getPossibleMoves(board);
		//Move(19, 27, 35, 18, 34, 33, 25, 17)
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 19));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 27));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 35));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 18));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 34));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 33));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 25));
		kingExceptedMoves.add(new Move.NormalMove(board, testKing, 17));

		//assert
		for (Move move: kingMoves) {
			System.out.println("Destination tile: " + move.getDestinationTile());
		}
		Assertions.assertTrue(kingExceptedMoves.size() == kingMoves.size());
	}

}

